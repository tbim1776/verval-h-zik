*** Keywords ***
Open Evomag
    [Documentation]  Opens evogmag.ro
    Open Browser    ${evomag}
    Sleep   1s
    Click Element   class:pushinstruments_button_deny
    Sleep   1s

Search for invalid item
    [Arguments]  ${item}
    Click Element   class:sn-suggest
    Sleep   1s
    Input Text  class:sn-suggest-input  ${item}
    Click Button    class:submit-search
    Sleep   2s
    Page Should Contain     CRITERIILE DE FILTRARE SELECTATE DE DUMNEAVOASTRA NU AU RETURNAT NICI UN REZULTAT!

Search for iPhone 7
    [Documentation]  Checks if all the search results are iPhone 7 related
    Input Text  class:sn-suggest-input  ${iphone}
    Click Button    class:submit-search

Check if iPhone 7 are shown
    ${szoveg}  Execute Javascript  {const elements=document.getElementsByClassName('npi_name');var i;for(i=0;i<elements.length;i++){if (!elements[i].textContent.toLowerCase().includes('${iphone}')){return 0;}} return 1;}
    Should Be True  ${szoveg}

Click on in stock filters
    Click Element   id:c19zdG9ja19zdGF0dXMmIzM0aW5fc3RvY19tYWdhemluJiMzNA__
    Click Element   id:c19zdG9ja19zdGF0dXMmIzM0aW5fc3RvY18oZXhjbHVzaXZfb25saW5lKSYjMzQ_
    Sleep   1s

Check if in stock is correct
    ${szoveg}  Execute Javascript  {const elements=document.getElementsByClassName('stoc_produs');var i;for(i=0;i<elements.length;i++){if (!elements[i].textContent.toLowerCase().includes('${stock}')){return 0;}} return 1;}
    Should Be True  ${szoveg}

Click on cover filter
    Click Element   id:YXR0cl9WR2x3JiMzNEJvb2sgQ292ZXImIzM0

Check if cover correct
    ${szoveg}  Execute Javascript  {const elements=document.getElementsByClassName('npi_name');var i;for(i=0;i<elements.length;i++){if (!elements[i].textContent.toLowerCase().includes('book cover') && !elements[i].textContent.toLowerCase().includes('protectie')){return 0;}} return 1;}
    Sleep   1s
    Should Be True  ${szoveg}

Log In To Evomag
    [Documentation]  Logs into my evoMag account
    Go To   ${evomag login}
    Input Text    id:LoginClientForm_Email  ${e-mail}
    Input Password  id:LoginClientForm_Password  ${password}
    Sleep   1
    Click Button    name:yt1

User logs in
    Log In To Evomag
    Sleep   1s

User adds random item to cart
    Click Link  https://www.evomag.ro
    Click Element  id:yw1
    Sleep  3s

User checks cart
    Click Element   class:btn
    Sleep   1s

Cart is not empty
    ${szoveg}    Execute Javascript  console.log(typeof document.getElementsByClassName("cart_article_row"));if(typeof document.getElementsByClassName("cart_article_row")[0]!=="undefined") return 1;return 0;
    Should Be True  ${szoveg}
    Sleep   1s

Change account details
    Go To   https://www.evomag.ro/client/details
    Input Text  id:Partner_Name     Ha Ha
    Input Text  id:Partner_Phone    +40749494949
    Input Text  id:checkCNP  1981919191919
    Click Element   id:save

Add iPhone 11 to wishlist
    Go To   https://www.evomag.ro/solutii-mobile-telefoane-mobile/apple-telefon-mobil-apple-iphone-11-lcd-ips-multi-touch-6.1-64gb-flash-camera-duala-12mp-wi-fi-4g-ios-alb-3764236.html
    Click Link  xpath://*[@id="add-to-cart-detail"]/div/div[3]/div[2]/div/a
    Sleep   1s
    Click Button    name:yt0
