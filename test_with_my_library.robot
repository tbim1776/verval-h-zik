*** Settings ***
Documentation  basic
Library  Selenium2Library
Library     Util.py
Resource  keywords.robot
Test Setup  Open Evomag
Test Teardown  Close Browser

*** Variables ***
${evomag login}     http://www.evomag.ro/client/auth
${evomag}   http://www.evomag.ro/
${e-mail}   tbokka@yahoo.com
${password}     sot675
${iphone}   iphone 7
${stock}        in stoc
${warranty page}    https://www.evomag.ro/client/frontWarranty
${details}     https://www.evomag.ro/client/details
${logout}   https://www.evomag.ro/client/logout

*** Test Cases ***
check if item loads
    [Documentation]     check if you click on an item it loads the proper item
    Search for iPhone 7
    Sleep   1s
    ${szoveg1}  Execute Javascript  element =  document.getElementsByClassName('npi_name')[0].textContent; return element.substring(0, element.indexOf(',')).toLowerCase().replace(/\s/g, '');
    Click Element   class:nice_product_container
    ${szoveg2}  Execute Javascript  element =  document.getElementsByClassName('product_name')[0].textContent; return element.substring(0, element.indexOf(',')).toLowerCase().replace(/\s/g, '');
    ${result}  Compare  ${szoveg1.strip()}  ${szoveg2.strip()}
    Should Be True  ${result}

Check if item is resigilat
    [Documentation]  Checks if an item is resigilat
    Go To   https://www.evomag.ro/resigilate-produse-resigilate/
    Click Link  xpath:/html/body/div[7]/div[4]/div[1]/div/div[2]/div[3]/div[6]/div[1]/div/div[3]/h2/a
    ${name}  Execute Javascript  return document.getElementsByClassName('product_name')[0].textContent;
    ${result}   Is Resigilat    ${name}
    Should Be True  ${result}

Check price filter
    [Documentation]     Filter is set
    Go To  https://www.evomag.ro/?sn.ff.f_price.to=2401&sn.ff.f_price.from=5&sn.q=iPhone+7
    Click Link  xpath:/html/body/div[7]/div[4]/div/div[2]/div[2]/div[5]/div[1]/div/div[3]/a
    ${price}  Execute Javascript  return document.getElementsByClassName('pret_rons')[0].textContent;
    ${result}   Is Cheaper Than 2401 Lei  ${price}
    Should Be True  ${result}

*** Keywords ***
