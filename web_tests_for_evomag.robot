*** Settings ***
Documentation  Homework for Verification and Validation, test are for evomag.ro
Library  Selenium2Library
Resource  keywords.robot
Test Setup  Open Evomag
Test Teardown  Close Browser
*** Variables ***
${evomag login}     http://www.evomag.ro/client/auth
${evomag}   http://www.evomag.ro/
${e-mail}   tbokka@yahoo.com
${password}     sot675
${iphone}   iphone 7
${stock}        in stoc
${warranty page}    https://www.evomag.ro/client/frontWarranty
${details}     https://www.evomag.ro/client/details
${logout}   https://www.evomag.ro/client/logout
${invalid}   dfgadfgajgfadsgfjadsgfa
${hungarian}     fényképezőgép
${not found}    vapor mini cu aburi
${sql injection}    ;DROP TABLE PRODUCTS;
*** Test Cases ***
Search for invalid items
    [Documentation]  Data driven test: Searches for some items and checks if the result appears
    [Template]  Search for invalid item
    ${hungarian}
    ${invalid}
    ${not found}
    ${sql injection}

check iPhone 7 search
    [Documentation]  Searches for iPhone 7, and check every item on the first page
    Search for iPhone 7
    Sleep   1s
    Check if iPhone 7 are shown

check stock filter
    [Documentation]  Checks if in stock filter works correctly
    Search for iPhone 7
    Sleep   1s
    Click on in stock filters
    Check if in stock is correct

check phone cover filter
    [Documentation]     Checks if phone cover filter works correctly
    Search for iPhone 7
    Sleep   1s
    Click on cover filter
    Check if cover correct

check adding to cart
    [Documentation]  Gherkin test: if the user logs in and adds something to the cart it shouldn`t be empty
    Given User logs in
    And User adds random item to cart
    When User checks cart
    Then Cart is not empty

#check if item loads
#    [Documentation]     check if you click on an item it loads the proper item
#    Search for iPhone 7
#    Sleep   1s
#    ${szoveg1}  Execute Javascript  element =  document.getElementsByClassName('npi_name')[0].textContent; return element.substring(0, element.indexOf(',')).toLowerCase().replace(/\s/g, '');
#    Click Element   class:nice_product_container
#    ${szoveg2}  Execute Javascript  element =  document.getElementsByClassName('product_name')[0].textContent; return element.substring(0, element.indexOf(',')).toLowerCase().replace(/\s/g, '');
#    Should Be Equal As Strings  ${szoveg1.strip()}  ${szoveg2.strip()}

check if subcategory loads
    [Documentation]     Checks if clicking the menu Laptopuri and Tablete loads in after clicking
    Click Link   xpath=//*[@id="menu_catex"]/div[2]/div/ul/li[1]/a
    Page Should Contain     Laptopuri si Tablete

check account information change
    [Documentation]  Changes account information, saves it and logs out, then checks if the operations succeeded
    Log In To Evomag
    Change account details
    Sleep   2s
    Go To   ${logout}
    Log In To Evomag
    Go To   ${details}
    Textfield Value Should Be   id:Partner_Name    Ha Ha

warranty test
    [Documentation]     with no items ordered, there shouldn`t be any warrantys
    Log In To Evomag
    Go To   ${warranty page}
    Page Should Contain     Nu ati facut nicio solicitare de service.

wishlist test
    [Documentation]     if we add an item to the wishlist it should appear. Testing it with an iPhone 11
    Log In To Evomag
    Add iPhone 11 to wishlist
    Page Should Contain     iPhone 11


*** Keywords ***
