class Util:
    def compare(self, a, b):
        return a == b

    def is_resigilat(self, str):
        return "Resigilat!" in str

    def is_cheaper_than_2401_lei(self, str):
        return int(str[0:len(str)-7].replace('.', '')) < 2401
